# deck

Il progetto è stato generato con [yo angular generator](https://github.com/yeoman/generator-angular)
versione 0.15.1. E' necessario installare tutte le dipendenze bower e nodejs.

## Installazione dipendenze

Installare le dipenze eseguendo `bower install` e `npm install`

## Build & development

Eseguire `grunt` per il building e `grunt serve` per l'anteprima.

## Testing

Running `grunt test` will run the unit tests with karma.
