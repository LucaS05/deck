'use strict';

/**
 * @ngdoc overview
 * @name deckApp
 * @description
 * # deckApp
 *
 * Main module of the application.
 */
var deckApp = angular.module('deckApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router'
]);

deckApp.config(['$stateProvider', '$urlRouterProvider', 'deckStarterProvider',
    function ($stateProvider, $urlRouterProvider, deckStarterProvider){
        deckStarterProvider.setPlayersNumber(10);
        deckStarterProvider.setCardsTemplateUrl('images/cards.svg');
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'views/home.html',
                controller: 'MainCtrl'
            });
    }
]);

deckApp.provider('deckStarter', function() {
    var playersNumber, templateUrl, maxCardNumber = 16;
    var semi = ["face", "stack", "tweet", "git"];

    this.setPlayersNumber = function(number){
        playersNumber = number;
    };
    this.setCardsTemplateUrl = function(url){
        templateUrl = url;
    };
    this.$get = function() {
        return {
            players: playersNumber,
            cardsTemplate: templateUrl,
            semi : semi,
            maxSemi: semi.length,
            maxCardNumber: 16
        };
    };
});