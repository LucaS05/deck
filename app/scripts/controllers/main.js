'use strict';

/**
 * @ngdoc function
 * @name deckApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the deckApp
 */
angular.module('deckApp').controller('MainCtrl', ['$scope', 'deckStarter', function ($scope, deckStarter){
    $scope.number = deckStarter.players;
    $scope.getNumber = function(num) {
        return new Array(num);
    };
}]);

/**
 * Il servizio è di supporto alle directive croupier e player.
 * Il nome indica appunto: CroupierPlayerService. Il servizio si occupa di tenere traccia dei giocatori attivi.
 * I giocatori non vengono mai eliminati dall'array players.
 * Infatti il metodo resetPlayer si occupa di ripristinare l'html dell'area all'indice index
 * reinserendo la label originale già presente nell'array.
 *
 * - addPlayer
 * Il metodo aggiunge un giocatore a quelli attivi. In particola le variabili pl e lbl
 * rappresentano gli elementi DOM dell'area del giocatore e della sua label.
 * Il metodo ritorna un indice che rappresenta il giocatore e può essere utilizzato all'esterno.
 *
 */

angular.module('deckApp').service('CPService', function(){
    var players = [], croupier, index = 0, activeIndex;
    this.addPlayer = function(pl, lbl){
        if(!angular.isUndefined(pl) && !angular.isUndefined(lbl)){
            var plObj = { area: pl, label: lbl}
            players.push(plObj);
            return index++;
        }
    };
    this.cpDragEnd = function(){
        var pl = players[activeIndex];
        if(!angular.isUndefined(pl)){
            if(pl.area.hasClass('player__area_active')){
                pl.area.removeClass('player__area_active');
            }
            if(pl.label.hasClass('player__label_active')){
                pl.label.removeClass('player__label_active');
            }
        }
    };
    this.setCard = function(card, index){
        var playerArea = players[index].area;
        if(!playerArea.hasClass('player__area_dropped')){
            playerArea.addClass('player__area_dropped');
            playerArea.html(card);
        }
    };
    this.resetPlayer = function(index){
        var label = players[index].label;
        var player = players[index];
        player.area.html(label);
        player.area.removeClass('player__area_dropped');
    };
    this.setActive = function(idx){
        activeIndex = idx;
    }
});

angular.module('deckApp').directive('cards', ['deckStarter', 'CardsService', function(deckStarter, cardsService) {
    return {
        restrict: 'A',
        transclude: true,
        templateUrl: deckStarter.cardsTemplate,
        link: function(scope, element){
            cardsService.setCards(element);
        }
    }
}]);

angular.module('deckApp').directive('croupier', ['CPService', 'CardsService', function(CPService, cardsService) {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: '<div id="croupier__area" draggable="true">\
                        <article id="card">\
                            <div class="cards" ng-transclude />\
                            <div class="card__wrapper">\
                                <svg width="100%" height="100%" class="card__svg" id="" />\
                            </div>\
                        </article>\
                   </div>',
        controller: ['$scope', function($scope) {

        }],
        link: function(scope, element){
            element.on({
                dragstart: function(ev) {
                    var card = cardsService.getCard();
                    var cardSvg = element.find(".card__svg");
                    cardSvg.html(card.svg.html());
                    cardSvg.attr("id", card.cardID);
                    var svgCard = element.find(".card__wrapper").html();
                    ev.originalEvent.dataTransfer.setData('text/html', svgCard);
                    element.css('opacity', '0.5');
                },
                dragend: function() {
                    CPService.cpDragEnd();
                    element.css('opacity', '1');
                }
            });
        }
    };
}]);

/**
 * Servizio per la gestione delle carte.
 * Il servizio si occupa di generare la carta appropriata da consegnare al giocatore.
 * Per carta appropiata s'intende una carta che non è stata già assegnata. Il metodo
 * che si occupa di effettuare questa operazione è il metodo getCard.
 *
 * - getCard
 *  Il metodo crea un oggetto card. Tale oggetto è composto da un id e da una proprietà
 *  chiamata svg. L'id viene anzitutto generato in automatico.
 *  Se l'id non è già stato assegnato allora viene creato un oggetto che conterrà
 *  l'id calcolato e l'svg ottenuto dall'elemento cards.
 *  cards contiene la struttura DOM del svg contenente tutte le carte.
 *  Se l'id è già stato utilizzato allora questo viene generato linearmente dal metodo
 *  di supporto getCardID.
 *  Il metodo sfrutta la variabile deck. deck è un array che modella il mazzo di carte.
 *  Ogni indice dell'array è il seme ottenuto dall'array semi.
 *  Per ogni indice viene creato un array che conterrà tutte le carte utilizzate per
 *  quel seme. Tale array avrà per ogni indice un booleano che indica se la carta n
 *  è stata utilizzata o meno.
 *
 *  - removeCard
 *  Il metodo elimina una carta dal mazzo. Utilizza un oggetto card composto da id e svg.
 *  Quindi l'id è nel formato "stack-1": dove stack indica il seme e 1 indica il numero
 *  della carta.
 *  Ottenuto il seme dalla stringa, verrà settata la carta 1 a false nell'array deck
 *  all'indice che rappresenta il seme "stack".
 */
angular.module('deckApp').service('CardsService', ['deckStarter', function(deckStarter){
    var cards;
    var semi = deckStarter.semi;
    var maxCardNumber = deckStarter.maxCardNumber;
    var maxSemi = deckStarter.maxSemi;
    var deck = {};

    semi.forEach(function(seme){
        deck[seme] = new Array();
    });

    this.setCards = function(cardsElement){
        cards = cardsElement;
    };

    var getCardID = function(semNumber, cardNumber, reset){
        var seme = semi[semNumber - 1];
        var cardsSeme = deck[seme];
        var card;
        if(cardsSeme[cardNumber - 1] !== undefined && cardsSeme[cardNumber - 1] !== false){
            if(cardNumber < deckStarter.maxCardNumber){
                card = getCardID(semNumber, cardNumber + 1, reset);
            } else if(semNumber < deckStarter.maxSemi, reset){
                card = getCardID(semNumber + 1, 1, reset);
            } else if(reset === false){
                reset = true;
                card = getCardID(1, 1, reset);
            } else {
                return -1;
            }
        } else {
            var cardsSeme = deck[seme];
            cardsSeme[cardNumber - 1] = true;
            card = semi[semNumber - 1] + "-" + cardNumber;
        }
        return card;
    };

    this.getCard = function(){
        var cardNumber = Math.floor(Math.random() * maxCardNumber ) + 1;
        var semeNumber = Math.floor(Math.random() * maxSemi) + 1;
        var seme = semi[semeNumber - 1];
        var cardsSeme = deck[seme];
        var cardID;
        var card = {};
        if(cardsSeme[cardNumber - 1] !== undefined){
            cardID = getCardID(semeNumber, cardNumber, false);
        } else{
            cardsSeme[cardNumber - 1] = true;
            cardID = seme + "-" + cardNumber;
        }
        card.svg = cards.find("#" + cardID);
        card.cardID = cardID;
        return card;
    };

    this.removeCard = function(card){
        if(!angular.isUndefined(card)){
            var cardID = card.split("-");
            var seme = cardID[0];
            var cardNumber = cardID[1];
            var cardsSeme = deck[seme];
            cardsSeme[cardNumber - 1] = false;
        }
    }
}]);

/**
 * Directive che rappresenta una singola area di un giocatore.
 * La directive nella sua funzione link, anzitutto aggiunge l'area e la label del giocatore
 * attraverso il servizio CPService. In seguito si occupa di gestire il dnd per ogni giocatore.
 * Il drag è disabilitato mentre tutti gli altri eventi sono gestiti.
 * In particolare nel metodo drop e modificato l'html dell'elemento player__area
 * inserendo l'svg della carta.
 * L'evento click invece si occupadi ripristinare l'html dell'area al suo stato originale
 * attraverso il metodo resetPlayer.
 */

angular.module('deckApp').directive('player', ['CPService', 'CardsService', function(CPService, cardsService) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="player col-md-6">\
                        <article class="player__area" draggable="true">\
                            <p class="player__label">Giocatore {{$index + 1}}</p>\
                        </article>\
                    </div>',
        link: function(scope, element){
            var player = element.find('.player__area');
            var label = element.find('.player__label');
            var playerIndex = CPService.addPlayer(player, label);
            player.on({
                dragstart: function(e) {
                    e.preventDefault();
                },
                dragleave: function() {
                    player.removeClass('player__area_active');
                    label.removeClass('player__label_active')
                },
                dragenter: function(e) {
                    e.preventDefault();
                    CPService.setActive(playerIndex);
                    player.addClass('player__area_active');
                    label.addClass('player__label_active')
                },
                dragover: function(e) {
                    e.preventDefault();
                    CPService.setActive(playerIndex);
                    player.addClass('player__area_active');
                    label.addClass('player__label_active')
                },
                drop: function(ev) {
                    var card = ev.originalEvent.dataTransfer.getData('text/html');
                    CPService.setCard(card, playerIndex);
                },
                click: function(){
                    var cardID = element.find("svg").attr("id");
                    cardsService.removeCard(cardID);
                    CPService.resetPlayer(playerIndex);

                }
            });
        }
    };
}]);